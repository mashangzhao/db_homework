--9.4.1
--a)
--give he name of a movis studio and networth

create or replace function StudioF(
	in s char(50),
	out presNetworth integer
) returns integer as $$

begin
	presNetworth := 0;
	select networth
	into presNetworth
	from studio,movieexec
	where studio.name = s and cert = presC;
end;
$$ language plpgsql;
select * from StudioF ('Paramount');

--b)

create function IsIn (
	in s char(50),
	in addr varchar(255)
) returns integer as $$
begin
	
	if exists (
		select * from MovieExec e,MovieStar ss
		where e.name = ss.name and e.name = s 
			and e.address = ss.address and e.address = addr)
	then return 3;
	elseif exists (select * from MovieStar where name = s and address = addr)
	then  return 1;
	elseif exists (select * from MovieExec where name = s and address = addr)
	then  return 2;
	else
		return 4;
	end if;
end;
$$ language plpgsql;
-- a moviestar
select * from IsIn ('Jane Fonda', 'Turner Av.');
-- a movieexec 'Dino De Laurentiis', 'Beverly Hills'
select * from IsIn ('Dino De Laurentiis', 'Beverly Hills');

--c)
-- find the two longest movies title
create or replace function TwoL (
	in s char(50),
	out T1 varchar(100),
	out T2 varchar(100)
) as $$
	declare t varchar(100);
	declare i int;
	declare moviecursor cursor for
		select title
		from movies
		where studioName = s 
		order by length desc
		limit 2 offset 0;
begin
	i := 0;
	T1 := null;
	T2 := null;
	open moviecursor;
	while (i < 2) do
		i := i + 1;
		if i = 1
		 fetch from moviecursor into T1;
		else 
		  fetch from moviecursor into T2;
		end if;
	end while;
	close moviecursor;
end;

$$ language plpgsql;

select * from TwoL('Fox');

--d)

--
create or replace function YearF (
	in s char(30),
	out early integer
) as $$
begin
	early := 0;
	select year 
	into early
	from movies,StarsIn
	where starName = s 
	and title = movietitle 
	and year = movieYear
	and length >= 120
	order by year;
	
end;
$$ language plpgsql;

 select * from yearf ('Mark Hamill');
 
 

 --e)
 create or replace function NameF (
	in addr varchar(255),
	out N char(30)
 ) as $$
 declare nc integer;
 
 begin
	nc := 0;
	select count(*)
	into nc
	from moviestar where address = addr;
	if nc = 1
		then 	select name into N from moviestar where address = addr;
	else 
		nc := null;
	end if;
end;
$$ language plpgsql;
select * from NameF ('Turner Av.');
select * from NameF ('Baldwin Av.');

--f)
-- get movietitle, delete from movies
--delete from StatsIn
create function DeleteS (
	in s char(30)
) returns void as $$
begin
	delete from movies 
	where title in (
		select Movietitle from StarsIn 
			where starName = s);
	delete from StarsIn where starName = s;
	delete from MovieStar where name  = s;
end;
$$ language plpgsql;

select * from DeleteS ('Kevin Spacey');

