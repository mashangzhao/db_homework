#7.1.1
-- a)
create database moviedb7;
\c moviedb7;
\d

create table Movieexec (
	name char (30),
	address varchar(255),
	cert int primary key,
	netWorth int
);

create table movies (
	title char (100),
	year int,
	length int,
	gener char (10),
	studioName char (30),
	producerC int references Movieexec(cert),
	primary key (title,year)
);
-- insert date to Movieexec and movies

insert into Movieexec values ('A','high road',1234,100);
insert into movies values ('abc',2016,120,'comedy','unknown',1234);

-- modification to Movieexec that violate this constraint

update Movieexec set cert = 2345 where name = 'A';

-- b)
drop table movies;
drop table Movieexec;

create table Movieexec (
	name char (30),
	address varchar(255),
	cert int primary key,
	netWorth int
);

create table movies (
	title char (100),
	year int,
	length int,
	gener char (10),
	studioName char (30),
	producerC int references Movieexec(cert) 
		on update set null,
	primary key (title,year)
);

-- insert date to Movieexec and movies

insert into Movieexec values ('A','high road',1234,100);
insert into movies values ('abc',2016,120,'comedy','unknown',1234);

-- modification to Movieexec and violate result and set producerC to NULL
update Movieexec set cert = 2345 where name  ='A';

-- watch result
select * from Movieexec;
select * from movies;
select producerC from movies where title  = 'abc';

-- c
drop table movies;
drop table Movieexec;

create table Movieexec (
	name char (30),
	address varchar(255),
	cert int primary key,
	netWorth int
);

create table movies (
	title char (100),
	year int,
	length int,
	gener char (10),
	studioName char (30),
	producerC int references Movieexec(cert) 
		on update cascade
		on delete cascade,
	primary key(title,year)
);
-- insert date 

insert into Movieexec values ('A','high road',1234,100);
insert into Movieexec values ('B','high street',5678,200);
insert into movies values ('abc',2016,120,'comedy','unknown',1234);
insert into movies values ('efg',2017,120,'comedy','unknown',5678);

-- update date to Movieexec and watch movies's producerC 

update Movieexec set cert = 888 where name = 'A';
select producerC from movies where title = 'abc';

-- delete date from Movieexec and watch the constraint for movies

delete from Movieexec where name  = 'B';

select * from Movieexec;
select * from movies;

--d)
drop table movies;
drop table Movieexec;

create table movies (
	title char (100),
	year int,
	length int,
	gener char (10),
	studioName char (30),
	producerC int,
	primary key(title,year)
);


create table StarsIn (
	movieTitle char(100) references movies(title),
	movieYear int,
	starName char(30),
	primary key(movieTitle,movieYear,starName)
);

-- it's wrong
-- ERROR:  there is no unique constraint matching given keys for referenced table "movies"

--e)

drop table movies;

create table MovieStar (
	name char(30) primary key,
	address varchar(255),
	gender char(1),
	birthdate date
);

create table StarsIn (
	movieTitle char(100),
	movieYear int,
	starName char(30) references MovieStar(name)
		on delete cascade,
	primary key(movieTitle,movieYear,starName)
);

-- insert data
insert into MovieStar values ('q','road1','f','2000-02-29');
insert into StarsIn values ('A',2016,'q');

select * from MovieStar;
select * from StarsIn;

delete from MovieStar where name  = 'q';
select starName from StarsIn;

-- 7.2.1
--a)
create table movies (
	title char(100),
	year int check (year > 1915),
	length int,
	gener char(10),
	studioName char(30),
	producerC int,
	primary key (title,year)
);

--insert data
insert into movies values ('A',2000,100,'comedy','Fox',1234);
insert into movies values ('B',1920,100,'comedy','Fox',1234);

-- ERROR:  new row for relation "movies" violates check constraint "movies_year_check"
insert into movies values ('C',1900,100,'comedy','Fox',1234);

--b)
drop table movis;
create table movies (
	title char(100),
	year int,
	length int check (length between 60 and 250),
	gener char(10),
	studioName char(30),
	producerC int,
	primary key (title,year)
);

--insert data
insert into movies values ('A',2000,80,'comedy','Fox',1234);
insert into movies values ('B',1920,150,'comedy','Fox',1234);

-- ERROR:  new row for relation "movies" violates check constraint "movies_length_check"
insert into movies values ('C',1900,40,'comedy','Fox',1234);

--c)
drop table movis;
create table movies (
	title char(100),
	year int,
	length int,
	gener char(10),
	studioName char(30) check (studioName in ('Disney','Fox','MGM','Paramount')),
	producerC int,
	primary key (title,year)
);
--insert data
insert into movies values ('A',2000,80,'comedy','Fox',1234);
insert into movies values ('B',1920,150,'comedy','Disney',1234);

-- ERROR:  new row for relation "movies" violates check constraint "movies_length_check"
insert into movies values ('C',1900,40,'comedy','EEEEE',1234);

-- 7.2.3

-- a)

create table MovieStar (
	name char(30) primary key,
	address varchar(255),
	gender char(1),
	birthdate Date
);
create table StarsIn (
	movieTitle char(100),
	movieYear int 
		check (movieYear > (select year(birthdate) from MovieStar)),
	starName char(30) references MovieStar(name),
	primary key(movieTitle,movieYear,starName)
);

-- ERROR:  cannot use subquery in check constraint

--b)
create table studio (
	name char(30) primary key,
	address varchar(255) check ( address is UNIQUE ),
	presc int 
);


-- insert data
insert into studio values ('A','road-1',123);
insert into studio values ('B','road-2',456);

insert into studio values ('C','road-1',789);

-- without check but unique fro address 
drop table studio;
create table studio (
	name char(30) primary key,
	address varchar(255) unique,
	presc int 
);
-- insert data
insert into studio values ('A','road-1',123);
insert into studio values ('B','road-2',456);
-- violate
insert into studio values ('C','road-1',789);
--ERROR:  duplicate key value violates unique constraint "studio_address_key"
--描述:  Key (address)=(road-1) already exists.

--c)
create table Movieexec (
	name char(30),
	address varchar(255),
	cert int primary key,
	netWorth int
);

create table MovieStar (
	name char(30) check (name in (select name from Movieexec)),
	address varchar(255),
	gender char(1),
	birthdate date,
	primary key (name)
);
--ERROR:  cannot use subquery in check constraint

--d)
create table movies (
	title char(100),
	year int,
	length int,
	gener char(10),
	studioName char(30),
	producerC int,
	primary key (title,year)
);

create table studio (
	name char(30) check (name in (select studioName from movies)),
	address varchar(255),
	presc int
);
-- ERROR:  cannot use subquery in check constraint

--e)
create table studio (
	name char(30) primary key,
	address varchar(255),
	presc int
);

create table movies (
	title char(100),
	year int,
	length int,
	gener char(10),
	studioName char(30),
	producerC int,
	primary key (title,year),
		check (producerC not in (select presc from studio) or
			(studioName in (select name from studio where presc  = producerC))
	)
);
--ERROR:  cannot use subquery in check constraint

--7.2.6

-- Example 7.6
CREATE TABLE moviestar_1 (
    name		CHAR(30) PRIMARY KEY,
    address		VARCHAR(255),
    gender		CHAR(1) CHECK (gender IN ('F', 'M')),
    birthdate	date
);

-- Example 7.8
create table moviestar_2 (
	name char(30) primary key,
	address varchar(255),
	gender char(1),
	birthdate date
	check (gender  = 'F' or name not like 'Ms.%')
);


-- insert data
insert into moviestar_1 values ('A','road_1','F','2000-02-03');
insert into moviestar_1 (name,address,birthdate) values ('B','road_1','2000-02-03');
insert into moviestar_2 values ('A','road_1','F','2000-02-03');
insert into moviestar_2 (name,address,birthdate) values ('B','road_1','2000-02-03');

--watch data
select * from moviestar_1;
select * from moviestar_2;

--7.3.1

--a)
create table movie (
	title char(30),
	year int,
	length int,
	gender int,
	studio char(100),
	producerC int
);
alter table movie add constraint movie_pk 
	primary key(title,year);

--b)
create table Movieexec (
	name char(30),
	address varchar(255),
	cert int primary key,
	netWorth int
);

create table movie (
	title char(30),
	year int,
	length int,
	gender int,
	studio char(100),
	producerC int unique
);

alter table movie add constraint movie_fk
	foreign key(producerC) references Movieexec(cert);
	
-- insert data
insert into movie values ('abc',2000,120,123456789,'G',123);
--ERROR:  insert or update on table "movie" violates foreign key constraint "movie_fk"
--描述:  Key (producerc)=(123) is not present in table "movieexec".
-- and then 
insert into Movieexec values ('A','road-1',123,1000);
insert into movie values ('abc',2000,120,123456789,'G',123);

--c)
--
create table movie (
	title char(30),
	year int,
	length int,
	gender int,
	studio char(100),
	producerC int
);

alter table movie add constraint length_k check (length between 60 and 250);

--insert data
insert into movie values('A',2000,100,'comedy','Fox',12345);
insert into movie values('A',2000,300,'comedy','Fox',12345);

--ERROR:  new row for relation "movie" violates check constraint "length_k"

--d)
create table MovieStar (
	name char(30),
	address varchar(255),
	gender char(1),
	birthdate date
);
create table Movieexec (
	name char(30),
	address varchar(255),
	cert int,
	netWorth int
);

alter table MovieStar add constraint star_name_c
	check (name not in (select name from Movieexec));

alter table Movieexec add constraint exec_name_c
	check (name not in (select name from MovieStar));
	
--ERROR:  cannot use subquery in check constraint

--e)
create table studio (
	name char(30) primary key,
	address varchar(255),
	presc int
);

-- 
alter table studio add constraint NO_Same_c
	check (name is unique);
-- and this
alter table studio add constraint NO_Same_c
	unique (name);
	
--7.5.4
--waiting

--a)
create table MovieStar (
	name char(30) primary key,
	address varchar(255),
	gender char(1),
	birthdate date
);

create table StarsIn (
	movieTitle char(100),
	movieYear int,
	starName char(30),
	primary key(movieTitle,movieYear,starName)
);

insert into StarsIn values ('A',2001,'ABC');
insert into MovieStar values ('DEF','high road','M','2001-01-01');


create function update_starname() returns trigger as $update_starname$
	begin
		insert into MovieStar(name)
			values (new.starName);
	return null;
	end;
$update_starname$ language plpgsql;

create trigger starsintrigger 
after update of starName on StarsIn
for each row
when (new.starName not in
		(select name from MovieStar))
execute procedure update_starname();

-- ERROR:  cannot use subquery in trigger WHEN condition
-- example but not support with subquery	
insert into StarsIn values ('A',2001,'AAA');

--b)
--update
create trigger movieexec_t1
after update on movieexec 
referencing 
	new row as newrow
for each row
when (newrow.cert not exits ((select presc from studio)
		union all 
	(select producerC from movies))
	)
insert into movies(producerC) values (newrow.cert);

--insert 
create trigger movieexec_t2
after insert on movieexec
referencing 
	new row as newrow
for each row
when (newrow.cert not exits ((select presc from studio )
		union all 
		(select producerC from movies))
	)
insert into movies(producerC) values (newrow.cert);
	
--c)

create trigger movie_t
after delete on MovieStar
referencing 
	old table as oldstaff
for each statement
when (1 > all (select count(*) from StarsIn s,MovieStar m
				where s.starName = m.name
					group by s.movieTitle,m.gender)
	)
insert into MovieStar 
	(select * from oldstaff);
	
--d)

create trigger moviesnum_t1
after insert on movies
referencing 
		new table as newstuff
for each statement
when (100 < all	
		(select count(*) from movies
			group by studioName,year)
	)
delete from movies
where (title,year,length,genre,studioName,procedure) in newstuff;

create trigger moviesnum_t2
after update on movies
referencing 
	old table as oldstuff,
	new table as newstuff
for each statement
when ( 100 < all
		(select count(*) from movies 
			group by studioName,year)
	)
begin	
	delete from movies
	where (title,year,length,gender,studioName,producerC) in newstuff;
	insert into movies 
		(select * from oldstuff);
end;
	
--e)

create trigger avglength_t
after update of length on movies
referencing 
	old table as oldstuff,
	new table as newstuff
for each statement
when (120 < (select avg(length) from movies))
begin
	delete from movies 
	where (title,year,length,gender,studioName,producerc) in newstuff;
	insert into movies 
		(select * from oldstuff);
end;
	