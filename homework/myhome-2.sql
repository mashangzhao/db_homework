-- 8.1.1
--a)

create view RichExec as
	select * from MovieExec
	where netWorth >= 10000000;
 -- select to test
 select * from richexec;

 --b)
create view StudioPres as
	select MovieExec.name as Ename,MovieExec.address as Eaddress,MovieExec.cert as Ecert
	from MovieExec,Studio
	where cert = presc;
select * from StudioPres;

--c)
create view ExecutiveStar as
	select MovieStar.name as Sname,
		   MovieStar.address as Saddress,
		   gender,
		   birthdate,
		   cert,
		   netWorth
	from MovieStar,MovieExec
	where MovieStar.name = MovieExec.name;
select * from ExecutiveStar;

--8.1.2
--a)

select Sname from ExecutiveStar where gender ='f'; 
--b)
select RichExec.name from RichExec,ExecutiveStar where RichExec.name = ExecutiveStar.Sname;

--c)
select StudioPres.ename 
from  RichExec,StudioPres,ExecutiveStar
where RichExec.netWorth >= 50000000;

--8.2.1
--可以更新的view(视图)一定是建立在单表上的视图，所以只有RichExec。

--8.2.2
--a)
--可以更新DisneyComedies视图

--b)
create table movies (
	title char(100),
	year int,
	length int,
	genre char(10),
	studioName char(30),
	producerC int,
	primary key(title,year)
);
--insert data
INSERT INTO Movies VALUES ('Logan''s run', 1976, NULL, 'sciFi', 'MGM', 123);
INSERT INTO Movies VALUES ('Star Wars', 1977, 124, 'sciFi', 'Fox', 555);
INSERT INTO Movies VALUES ('Empire Strikes Back', 1980, 111, 'fantasy', 'Fox', 555);
INSERT INTO Movies VALUES ('Star Trek', 1979, 132, 'sciFi', 'Paramount', 345);
INSERT INTO Movies VALUES ('Star Trek: Nemesis', 2002, 116, 'sciFi', 'Paramount', 345);
INSERT INTO Movies VALUES ('Terms of Endearment', 1983, 132, 'romance', 'MGM', 123);
INSERT INTO Movies VALUES ('The Usual Suspects', 1995, 106, 'crime', 'MGM', 456);
INSERT INTO Movies VALUES ('Gone With the Wind', 1938, 238, 'drama', 'MGM', 123);
INSERT INTO Movies VALUES ('Wayne''s World', 1992, 95, 'comedy', 'Paramount', 123);
INSERT INTO Movies VALUES ('King Kong', 2005, 187, 'drama', 'Universal', 789);
INSERT INTO Movies VALUES ('King Kong', 1976, 134, 'drama', 'Paramount', 666);
INSERT INTO Movies VALUES ('King Kong', 1933, 100, 'drama', 'Universal', 345);
INSERT INTO Movies VALUES ('Pretty Woman', 1990, 119, 'comedy', 'Disney', 999);

create view DisneyComedies as 
	select title,year,length from Movies
	where studioName = 'Disney' and genre = 'comedy';

create function DisneyCommedyInsert() returns trigger as $disynecomedy_insert$	
	begin
		insert into movies(title,year,length,studioName,genre)
		values(new.title,new.year,new.length,'Disney','comedy');
		return new;
	end;
$disynecomedy_insert$ language plpgsql;

create trigger DisneyCommedyInsert_T
instead of insert on DisneyComedies
for each row
execute procedure DisneyCommedyInsert();

--insert a record and select 
insert into disneycomedies values ('a',2016,120);
select * from movies;

--c)

create function LengthUpdate() returns trigger as $Length_Update$
	begin
		update movies set length =  new.length 
		where title =new.title and year =new.year ;
		return new;
	end;
$Length_Update$ language plpgsql;

-- a trigger 
create trigger Length_T
instead of update on DisneyComedies
for each row
execute procedure LengthUpdate();
--update
update DisneyComedies set length = 222 where title = 'a' and year = 2016;
--test,it's ok
select * from DisneyComedies;

--8.4.1 
--

--8.5.1
--change title to char(50) from char(100) to see every record at a line
alter table movies alter column title  type char(50);

-- update movies(title,year) 

update movieprod set title = 'xxx' where title = 'oldtitle' and year  = 'oldyear';
update movieprod set year = 2020 where title = 'oldtitle' and year  = 'oldyear';

-- update to movieexec involving cert#
--first delete and then insert
delete from movieprod where (title,year) in (
	select title,year
	from movies,movieexec 
	where cert  = oldcert and cert  = producerC
);

insert into movieprod
	select title,year,name 
	from movies,movieexec
	where cert  = oldcert and cert  = producerC;
	
--8.5.4
--select starsname who are in a movie where producer 
select starName 
from starsIn,movies,movieexec
where movietitle = title 
		and movieyear = year 
		and name  = 'Max Bialystock';

--select  a movie
select title,year
from movies,movieexec
where producerC = cert and name = 'Max Bialystock';

--
select name 
from movies,movieexec,starsIn
where producerc  = cert 
	and title = movietitle
	and year = movieyear
	and starName = 'arther';

-- the count of movies produce by arther
select count(*)
from movies,movieexec
where producerc = cert
	and name = 'artherer';

-- the count of movies group by name from producerC
select count(*)
from movies,movieexec
where producerc = cert group by name;