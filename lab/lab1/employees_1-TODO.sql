﻿
-- Create the database first.

CREATE DATABASE Employees;

\c Employees; -- PostgreSQL

-- 按照实验报告要求编写SQL语句

-- 3
-- 用SQL创建关系表
--employees
create table employees(
	emp_no int,
	birth_date char(10),
	first_name char(30),
	last_name char(30),
	gender char(1),
	hire_date char(10)
);
--add primary key(emp_no)
alter table employees add constraint pk_employees primary key(emp_no);

-- create table salaries
create table salaries(
	emp_no int,
	salary int,
	from_date char(10),
	to_date char(10)
);
-- add primary key
alter table salaries 
	add constraint pk_salaries 
	primary key(emp_no,from_date,to_date);
--add foreign key(emp_no) references employees(emp_no)
alter table salaries 
	add constraint fk_salaries_employees 
	foreign key(emp_no) references employees(emp_no);
	
-- create table titles
create table titles(
	emp_no int,
	title varchar(30),
	from_date char(10),
	to_date char(10)
);
-- add primary key and foreign key
alter table titles 
	add constraint pk_titles primary key(emp_no,title,from_date,to_date);

alter table titles 
	add constraint fk_titles_employees 
	foreign key(emp_no) references employees(emp_no);

-- departments
create table departments(
	dept_no char(4),
	dept_name varchar(30)
);
--add primary key
alter table departments add constraint pk_departments primary key(dept_no);

--create tbale dept_emp
create table dept_emp (
	emp_no int,
	dept_no char(4),
	from_date char(10),
	to_date char(10)
);
--add primary key and foreign key
alter table dept_emp 
	add constraint pk_dept_emp primary key(emp_no,dept_no,from_date,to_date);
alter table dept_emp 
	add constraint fk_dept_emp_employees 
	foreign key(emp_no) references employees(emp_no);
alter table dept_emp 
	add constraint fk_dept_emp_departments 
	foreign key(dept_no) references departments(dept_no);

-- creat table dept_manager
create table dept_manager(
	dept_no char(4),
	emp_no int,
	from_date char(10),
	to_date char(10)
);
alter table dept_manager 
	add constraint pk_dept_manager primary key(dept_no,emp_no,from_date,to_date);
alter table dept_manager 
	add constraint fk_dept_manager_employees 
	foreign key(emp_no) references employees(emp_no);
alter table dept_manager 
	add constraint fk_dept_manager_departments 
	foreign key(dept_no) references departments(dept_no);

	
-- 测试创建的空表
SELECT * FROM employees;
SELECT * FROM departments;
SELECT * FROM dept_emp;
SELECT * FROM dept_manager;
SELECT * FROM titles;
SELECT * FROM salaries;

-- 4 导入数据
-- copy  data_employees
copy employees from 'c:/Users/arther/Desktop/lab/data_employees.txt' with delimiter as ',';
-- copy date_salaries
copy salaries from 'c:/users/arther/Desktop/lab/data_salaries.txt' with delimiter as ',';
-- copy data_titles
copy titles from 'c:/users/arther/Desktop/lab/data_titles.txt' with delimiter as ',';
-- copy date_departments
copy departments from 'c:/users/arther/Desktop/lab/data_departments.txt' with delimiter as ',';
-- copy dept_emp
copy dept_emp from 'c:/users/arther/Desktop/lab/data_dept_emp.txt' with delimiter as ',';
-- copy dept_manager
copy dept_manager from 'c:/users/arther/Desktop/lab/data_dept_manager.txt' with delimiter as ',';

-- 测试导入数据之后的表
SELECT COUNT(*) FROM employees;
SELECT COUNT(*) FROM departments;
SELECT COUNT(*) FROM dept_emp;
SELECT COUNT(*) FROM dept_manager;
SELECT COUNT(*) FROM titles;
SELECT COUNT(*) FROM salaries;

-- 5.1 返回前十行员工数据
select * from employees limit 10 offset 0;

--5.2 查询first_name为Peternela且last_name为Anick的员工的编号、出生日期、性别和入职日期。
select emp_no,birth_date,gender,hire_date
	from employees 
	where first_name = 'Peternela' and last_name = 'Anick';

--5.3 查询出生日期在1961-7-15（包括）到1961-7-20（包括）之间的员工的编号、姓名和出生日期。
select emp_no,first_name,last_name
	from employees
	where to_date(birth_date,'YYYY-MM-DD') between '1961-7-15' and '1961-7-20';

--5.4 查询所有first_name中含有前缀Peter或last_name中含有前缀Peter的员工数据（返回所有列）。
select * from employees where first_name like 'Peter%' or last_name like 'Peter%';	

--5.5 查询工资数额的最大值，并将查询结果的列名命名为max_salary。
select max(salary) max_salary from salaries;

--5.6 查询部门编号及相应部门的员工人数，
--		并按照部门编号由小到大的顺序排序（将员工人数列命名为dept_emp_count）。
select dept_no,count(*) dept_emp_count
	from dept_emp 
	group by dept_no 
	order by dept_no asc;

-- 5.7 查询员工“Peternela Anick”的员工编号、所在部门编号和在该部门的工作起始时间。
select d.emp_no emp_no,dept_no,from_date
	from dept_emp d,employees e
	where first_name = 'Peternela' and last_name = 'Anick' and d.emp_no = e.emp_no;

-- 5.8 查询姓名相同的员工x和员工y的编号和姓名（只列出前10行结果）
select e1.emp_no as emp_no,e2.emp_no as emp_no,
		e1.first_name,e1.last_name
	from employees e1,employees e2 
	where e1.emp_no <> e2.emp_no
		and e1.first_name = e2.first_name 
		and e1.last_name = e2.last_name
	order by e1.emp_no asc
	limit 10 offset 0;


-- 5.9 查询姓名为“Margo Anily”的员工编号和
-- 出生日期为“1959-10-30”且入职日期为“1989-09-12”的员工编号的并集。
(select emp_no 
	from employees
	where first_name = 'Margo' and last_name = 'Anily')
	union
(select emp_no
	from employees
	where birth_date = '1959-10-30' and hire_date = '1989-09-12');



-- 5.10 查询员工“Margo Anily”所在的部门的名称（要求用子查询实现）。
select dept_name
	from  departments,dept_emp
	where departments.dept_no = dept_emp.dept_no
	and dept_emp.emp_no in
		(select employees.emp_no 
			from employees 
			where first_name = 'Margo' and last_name = 'Anily' );



		
-- 5.11 要求用JOIN…ON连接语法实现查询5.10。
select dept_name
	from departments join dept_emp on
		departments.dept_no  = dept_emp.dept_no 
		join employees on
			dept_emp.emp_no = employees.emp_no
			where first_name = 'Margo' and last_name = 'Anily';



-- 5.12 查询在全部部门中工作过的员工的编号和姓名（提示：用NOT EXISTS连接的子查询）。
select emp_no,first_name,last_name
	from employees
	where not exists
	(select * 
		from departments
		where not exists
			(select * 
				from dept_emp 
				where  dept_emp.emp_no = employees.emp_no 
					and dept_emp.dept_no = departments.dept_no)
	);



-- 5.13 查询员工人数大于等于50000的部门编号、部门名称和部门员工人数，
--  按照部门编号由小到大的顺序排序（将部门员工人数列命名为dept_emp_count）。
select d.dept_no,dept_name,count(*) dept_emp_count
	from departments d,dept_emp,employees e
	where d.dept_no = dept_emp.dept_no and dept_emp.emp_no = e.emp_no 
	group by d.dept_no
	having count(*) >= 50000
	order by d.dept_no asc;



-- 5.14 在员工表中添加一行记录：
-- (10000, 1981-10-1, Jimmy, Lin, M, 2011-12-8)
insert into employees values (10000, '1981-10-1', 'Jimmy', 'Lin', 'M', '2011-12-8');

-- 5.15 将5.14添加的员工记录的first_name属性值修改为Jim。
update employees set first_name = 'Jim' where emp_no = 10000;

-- 5.16 删除5.14添加的员工记录。
delete from employees where emp_no = 10000;

-- 5.17 在员工表中添加一行记录：
-- (10001, 1981-10-1, Jimmy, Lin, M, 2011-12-8)，观察执行输出结果。
insert into employees values (10001, '1981-10-1', 'Jimmy', 'Lin', 'M', '2011-12-8');
--ERROR:  duplicate key value violates unique constraint "pk_employees"
--描述:  Key (emp_no)=(10001) already exists.

-- 5.18 删除编号为10001的员工，观察执行输出结果。
delete from employees where emp_no = 10001;
--ERROR:  update or delete on table "employees" violates foreign key constraint "fk_salaries_employees" on table "salaries"
--描述:  Key (emp_no)=(10001) is still referenced from table "salaries".

